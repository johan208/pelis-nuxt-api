const hapi = require('@hapi/hapi');
const path = require('path');
const inert = require('@hapi/inert');
const db = require('./config/database');
const routes = require('./routes');

const init = async () => {

    const server = await hapi.Server({
        port:4000,
        host:'localhost',
        routes: {
            files: {
                relativeTo:path.join(__dirname, 'public')
            },
            cors:true
        }

    });
    
    await server.register(inert);  //plugin static files
    await server.register(routes); //plugin rutas
    await server.start();
    db();   //connectar base de datos
    console.log('server corriendo en ',server.info.uri);

};

init();

