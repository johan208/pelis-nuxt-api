
const User = require('../models/User')
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');

process.env.SECRET_KEY = 'secret';

exports.createUser = async (req, res) => {

    try {

        // extraer email y password
        const { email, password } = req.payload;
        console.log(req.payload)
        //revisar si ya hay el correo registrado
        let user = await User.findOne({ email });
        if(user) {
            return res.response({msj: "El usuario ya existe"}).code(400);
        }
        //Creo nuevo usuario
        user = new User(req.payload);

        //hashear contraseña
        const salt = await bcryptjs.genSalt(10);
        user.password = await bcryptjs.hash( password, salt );

        // guardar el usuario nuevo
        await user.save();

        const payload = {
            user: {
                id: user._id
            }
        };

        const token = jwt.sign(payload,process.env.SECRET_KEY,{
            expiresIn: 60 * 60 * 2
        });

        return res.response({token}).code(200);

    } catch(err) {
        console.error(err);
        res.response(err).code(400);
    }
    
}