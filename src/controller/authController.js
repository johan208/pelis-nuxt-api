const User = require('../models/User')
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');

exports.authUser = async (req, res) => {

    // extraer el email y pass
    const { email, password } = req.payload;

    try {
        //revisar correo registrado
        let user = await User.findOne({ email }) //{email:email} === {email}
        console.log(user);

        if(!user) {
            return res.response({msg: 'El usuario no existe'}).code(400);
        }
        //revisar pass hasheado
        const isPassOk = await bcryptjs.compare(password, user.password);
        if( !isPassOk ) {
            return res.response({msg: 'Password incorrecto'}).code(400);
        }

        const payload = {
            user: {
                id: user._id
            }
        }
        //firmar jwt
        let token = jwt.sign(payload,process.env.SECRET_KEY,{
            expiresIn:60 * 60 * 2
        })

        return res.response({token})

    } catch(err) {
        console.error(err);
        res.response('Hubo un problema').code(400);
    }

}

exports.getUser = async (req, res) => {
    try {
        const token = req.headers.authorization;
        
        // Revisar si no hay token
        if(!token) {
            return res.status(401).json({msg: 'Permiso no valido'})
        }
        const hash = jwt.verify(token, process.env.SECRET_KEY);
        req.user = hash.user;

        const user = await User.findById(req.user.id).select('-password');
        return res.response({user});
        
    } catch(err) {
        console.error(err);
        res.response('Hubo un problema').code(400);
    }
}