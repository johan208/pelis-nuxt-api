const Favorites = require('../models/Favorites')
const jwt = require('jsonwebtoken');

exports.newFavorite = async (req, res ) => {

    try {
        const {idMovieDB} = req.payload;
        
        //leer el token del header -- enviar token desde header
        const token = req.headers.authorization;
        
        // Revisar si no hay token
        if(!token) {
            return res.status(401).json({msg: 'Permiso no valido'})
        }
        const hash = jwt.verify(token, process.env.SECRET_KEY);
        console.log(hash);
        req.user = hash.user;

        //revisar si ya hay el idMovieBD registrado
        let favorite = await Favorites.findOne({ idMovieDB });
        if(favorite) {
             // Verificar el creador del proyecto
            // if(favorite.creator.toString() !== req.user.id) {
            //     return res.response({msg: 'No autorizado'}).code(401)
            // }
            // await Favorites.findOneAndRemove({ _id: favorite._id });
            return res.response({msj: "El idMovieBD ya existía"}).code(400);
        }
        //Creo un objeto con la info recibida del cliente
        favorite = new Favorites(req.payload);
        // Guardar el creador via JWT
        favorite.creator = req.user.id;
        await favorite.save();

        return res.response({favorite});

    } catch (e){
        console.error(e);
        res.response(e).code(500);
    }
}

exports.getFavorites = async (req, res ) => {
    try {
        //leer el token del header -- enviar token desde header
        const token = req.headers.authorization;
        // Revisar si no hay token
        if(!token) {
            return res.response({msg: 'Permiso no valido'}).code(401);
        }
        const hash = jwt.verify(token, process.env.SECRET_KEY);
        req.user = hash.user;
        const favorites = await Favorites.find({creator: req.user.id}).sort({created:'desc'});
        return res.response({ favorites });
    } catch(e) {
        console.error(e);
        res.response(e).code(500);
    }
}

exports.deleteFavorite = async (req, res ) => {
    try {
        const token = req.headers.authorization;
        
        // Revisar si no hay token
        if(!token) {
            return res.status(401).json({msg: 'Permiso no valido'})
        }
        const hash = jwt.verify(token, process.env.SECRET_KEY);
        req.user = hash.user;
        console.log(req.user)
        // Revisar id
        let favorite = await Favorites.findById(req.params.id);
        // Si el proyecto existe o no
        if(!favorite) {
            return res.response({msg: 'Favorito no existe'}).code(404);
        }
        // Verificar el creador del proyecto
        if(favorite.creator.toString() !== req.user.id) {
            return res.response({msg: 'No autorizado'}).code(401)
        }
        await Favorites.findOneAndRemove({ _id: req.params.id });

        return res.response({ favorite,msg: 'Proyecto eliminado'});

    } catch(e) {
        console.error(e);
        res.response(e).code(500);
    }
}
