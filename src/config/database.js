const mongoose = require('mongoose');

const connectDB = async () => {

    try {
        mongoose.connect('mongodb+srv://johan208:jgarci46@cluster0-vzkiv.mongodb.net/nuxtpelis',{
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify:false,
            useCreateIndex: true
        });
        console.log('Database is Connected');
    } catch(err) {
        console.error(err);
        process.exit(1); // detener la app en caso de error
    }   

};

module.exports = connectDB; 
