const Joi = require('@hapi/joi');
const jwt = require('jsonwebtoken');
const userController = require('../controller/userController');
const authController = require('../controller/authController');
const favoritesController = require('../controller/favoritesController');

module.exports = {
    name: 'apiRoutes',
    register: async (server, options, next ) => {
      server.route([
        {
          method:'POST',
          path: '/user',
          options: {
            validate: {
              payload: Joi.object({
                name: Joi.string().min(4).required(),
                email: Joi.string().email().required(),
                password:Joi.string().min(6).required(),
                // registry: Joi.date()
              }),
              failAction: (req, res, error ) => {
                return error.isJoi
                      ? res.response(error.details[0]).takeover()
                      : res.response(error).takeover()
              }
            }
          },
          handler: userController.createUser
        },
        {
          method:'GET',
          path:'/user',
          handler:authController.getUser
        },
        {
          method:'POST',
          path:'/login',
          options: {
            validate: {
              payload: Joi.object({
                email: Joi.string().email().required(),
                password:Joi.string().min(6).required(),
              }),
              failAction: (req, res, error ) => {
                return error.isJoi
                      ? res.response(error.details[0]).takeover()
                      : res.response(error).takeover()
              }
            }
          },
          handler:authController.authUser
        },
        {
          method:'POST',
          path:'/favorites',
          options: {
            validate: {
              payload: Joi.object({
                idMovieDB: Joi.number().integer().required(),
                name: Joi.string().required()
              }),
              failAction: (req, res, error ) => {
                return error.isJoi
                      ? res.response(error.details[0]).takeover()
                      : res.response(error).takeover()
              }
            }
          },
          handler:favoritesController.newFavorite
        },
        {
          method:'GET',
          path:'/favorites',
          handler:favoritesController.getFavorites
        },
        {
          method:'DELETE',
          path:'/favorites/{id}',
          // options: {
            
          // },
          handler:favoritesController.deleteFavorite
        }
      ]);
    }
  }