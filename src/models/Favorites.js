const mongoose = require('mongoose');

const FavoriteSchema = mongoose.Schema({
    idMovieDB: {
        type: Number,
        required: true,
        trim: true,
        unique:true
    },
    name: {
        type: String,
        required: true,
        trim: true
    },
    creator : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    created : {
        type:Date,
        default: Date.now()
    }
})

module.exports = mongoose.model('Favorites',FavoriteSchema)